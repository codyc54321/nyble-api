import json


def get_response_json(response):
    str_response = response.readall().decode('utf-8')
    data = json.loads(str_response)
    return data



"""
Model level
"""

def check_existence(model, data):
    if type(data) == dict:
        pass
    else:
        try:
            data = json.loads(data)
        except TypeError:
            raise Exception('This function requires JSON or python dictionary')

    queryset = model.objects.filter(**data)

    return len(queryset) > 0


def delete_all(model):
    objects = model.objects.all()
    for obj in objects:
        obj.delete()

"""
END Model level
"""
