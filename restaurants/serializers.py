from rest_framework import serializers

from restaurants.models import Restaurant, RestaurantCategory


class RestaurantSerializer(serializers.ModelSerializer):

    # http://stackoverflow.com/questions/17280007/retrieving-a-foreign-key-value-with-django-rest-framework-serializers
    # category = serializers.ReadOnlyField(source='restaurant_category.description', read_only=True) <== in case you need to serialize a FK attribute
    # this works here because of the @property 'category'
    
    class Meta:
        model = Restaurant
        fields = ('name', 'slug', 'category', 'address_line_1', 'address_line_2', 'city', 'state_code', 'zip_code', 'telephone_number')
        

class RestaurantCategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = RestaurantCategory
        fields = ('id', 'description',)
