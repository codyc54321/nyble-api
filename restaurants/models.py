from __future__ import unicode_literals

from django.db import models
from django.utils.text import slugify


class RestaurantCategory(models.Model):
    description = models.CharField(max_length=80)
    
    def __unicode__(self):
        return self.description
    

class Restaurant(models.Model):
    name = models.CharField(max_length=250)
    slug = models.CharField(max_length=250, blank=True)
    restaurant_category = models.ForeignKey(RestaurantCategory, blank=True, null=True)
    address_line_1 = models.CharField(max_length=255, blank=True)
    address_line_2 = models.CharField(max_length=255, blank=True)
    city = models.CharField(max_length=255, blank=True)
    state_code = models.CharField(max_length=2, blank=True)
    zip_code = models.CharField(max_length=15, blank=True)
    telephone_number = models.CharField(max_length=20, blank=True)
    
    @property
    def category(self):
        return self.restaurant_category.description
    
    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)
        
    def __unicode__(self):
        return self.name