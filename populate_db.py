#!/usr/bin/env python

"""
http://stackoverflow.com/questions/35226883/python-django-typeerror-relative-imports-require-the-package-argument
"""

import django, os
from django.core import serializers
# from django.forms.models import model_to_dict

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nyble-api.settings")
django.setup()

import django.apps

models = django.apps.apps.get_models()

# user = User.objects.create(email='cchilder@mail.usf.edu', username='cchilder@mail.usf.edu', first_name='Cody', last_name='Childers', password='childers9')
# user.save()


DATA = {
        'User': [
            {'email': 'test@example.com', 'username': 'test@example.com', 'first_name': 'John', 'last_name': 'Doe', 'password': 'password'},
        ]
}



for Model in models:
    model_name = Model.__name__
    try:
        object_definitions = DATA[model_name]
        for d in object_definitions:
            this_object = Model(**d)
            this_object.save()
    except:
        pass



#
# BANNED_NAMES = ['Permission', 'ContentType']
#
# FIELD_MAPPER = {'model': 'User', 'fields': ('email', 'password')}
#
# # http://stackoverflow.com/questions/2170228/iterate-over-model-instance-field-names-and-values-in-template
# for Model in models:
#     model_name = Model.__name__
#     if model_name in BANNED_NAMES:
#         continue
#     if Model.objects.count():
#         print('\nObjects in {class_name}:\n'.format(class_name=model_name))
#         objects = Model.objects.all()
#         for obj in objects:
#             data = serializers.serialize('python', objects, fields=('name','size'))
#             import ipdb; ipdb.set_trace()
