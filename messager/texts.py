# coding: utf-8

from twilio.rest import TwilioRestClient

NUMBERS_MAP = {
    # 'las_trancas': "+18139567640",
    'las_trancas': "+15127365653",
}


def send_text(data, format_callback):
    accountSID = "ACc610241f35eb41f9cfea0bf9a0040d89"
    authToken  = "06f908909a727eec51806398daf02e6d"

    client = TwilioRestClient(accountSID, authToken)

    twilio_number = "+15123611761"

    target_number = NUMBERS_MAP[data['restaurant']]

    formatted_body = format_callback(data)
    print(formatted_body)
    message = client.messages.create(body=formatted_body, from_=twilio_number, to=target_number)
