#!/usr/bin/env python
# coding: utf-8

import os, sys, subprocess, time, re
import yagmail

from ez_scrip_lib import read_content

myemail = 'cchilder@mail.usf.edu'

default_contents = ["I don't know of a system that exists, but I can write one real quick",
            'You can find a resume attached.',
            'each list item starts on new line',
            'somehow it knows the file is a file, probably tries to open each string as file to determine',
            '/home/cchilders/Downloads/Cody-Childers-Resume.docx', 'it knows this is just text regardless of the order']
        
        
def send_email(email_contents, person_to=myemail, subject="automated email tests"):
    yag = yagmail.SMTP(myemail, os.environ["GMAILPW"])
    yag.send(person_to, subject, email_contents)
    
    
def send_rapid_emails(email_contents=["hi"], amount_to_send=1, person_to=myemail, subject="automated email tests"): 
    email_contents.append('sending {} emails in under 0.1secs'.format(str(amount_to_send)))
    for i in range(0, (amount_to_send)):
        send_email(email_contents, person_to=person_to)
    
    
def send_delayed_email(minutes):
    seconds = minutes * 60
    time.sleep(seconds)
    
    
def get_internal_contents():
    fpath = get_current_filepath()
    this_content = read_content(fpath)
    return this_content
    
    
def send_self(person_to=myemail):
    fpath = get_current_filepath()
    file_contents = get_internal_contents()
    linelist = file_contents.split('\n')
    linelist = [file_contents, fpath]
    linelist.append("automatically sends this file, as in the file I'm in right now")
    send_email(linelist, person_to=person_to) # subject="this is the code for first test"


def get_current_filepath():
    filename = __file__.replace('./', '')
    return os.path.join(os.path.abspath(os.path.dirname(__file__)), filename)


fpath = get_current_filepath()
file_contents = get_internal_contents()


# send_rapid_emails(email_contents=default_contents, amount_to_send=6, person_to=myemail)
send_self(person_to=myemail)

