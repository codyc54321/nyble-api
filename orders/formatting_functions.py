import json

PLURAL_MAPPER = {
    'taco': 'tacos',
    'burrito': 'burritos',
    'quesadilla': 'quesadillas',
}


def add_line(item, add_on, new_lines=2):
    """ defaults to double-spaced new lines (a line break with an empty line after it) """
    new_line_characters = '\n' * new_lines
    new_line = add_on + new_line_characters
    if isinstance(item, list):
        item.append(new_line)
    elif isinstance(item, str):
        item += item + add_on + new_lines
    else:
        raise Exception('add_line() requires a list or string')
    return item


def get_item_line(keys_in_order, item_info_dict):
    string = ""
    plural = item_info_dict['quantity'] > 1
    for key in keys_in_order:
        try:
            if key == 'category':
                if plural:
                    string += str(PLURAL_MAPPER[item_info_dict[key]]) + ' '
                else:
                    string += str(item_info_dict[key]) + ' '
            else:
                string += str(item_info_dict[key]) + ' '
        except KeyError:
            pass
    return string


#TODO: break this into generic format_callback and pass in Las Trancas, Madam Mams, etc, requirements
def las_trancas_format_callback(request):
    if isinstance(request, str):
        python_obj = json.loads(json_request)
    else:
        python_obj = request
    order = python_obj['order']
    sorted_order = sorted(order, key=lambda k: k['category'])
    lines = []
    for item in sorted_order:
        keys_order = ['quantity', 'category', 'wrapping', 'ingredients', 'filling']
        line = get_item_line(keys_order, item)
        line += '\n\n'
        lines.append(line)
    lines.append(python_obj['customer_name'] + '\n\n')
    lines.append(python_obj['customer_phone_number'])
    return "".join(lines)


FUNCTIONS = {'las_trancas': las_trancas_format_callback}
