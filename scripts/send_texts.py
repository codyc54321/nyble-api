#!/usr/bin/env python3

from nyble-api.messager.texts import send_text
from orders.formatting_functions import las_trancas_format_callback


JSON = {"customer_name": "Cody Childers", "order": [{"categoria": "taco", "ingredientes": "todos", "cantidad": 1, "relleno": "barbacoa", "envoltorio": "harina"},\
    {"categoria": "taco", "ingredientes": "todos", "cantidad": 2, "relleno": "pollo", "envoltorio": "harina"}, {"categoria": "burrito", "ingredientes": "todos", \
    "cantidad": 1, "relleno": "pollo"}], "customer_phone_number": "8135452150", "restaurant": "las_trancas"}

send_text(JSON, las_trancas_format_callback)
