
# create a test user
from utils import check_existence
x = AppUser(first_name='Cody', last_name='Childers', email='cchilder@mail.usf.edu', password='green party FTW', phone='8135452150')
x.save()
d = {'first_name': 'Cody', 'last_name': 'Childers', 'email': 'cchilder@mail.usf.edu', 'password': 'green party FTW', 'phone': '8135452150'}
y = AppUser.objects.filter(**d)
user_exists = check_existence(AppUser, d)
assert user_exists == True


# reset users

def reset_table(model):
    from utils import delete_all
    delete_all(model)
    objects = model.objects.all()
    assert len(objects) == 0

reset_table(AppUser)


# create a restaurant
x = RestaurantCategory(description='food-truck')
x.save()
y = Restaurant(name='Las Trancas', address_line_1='1210 E Cesar Chavez St', city='Austin', state_code='TX',
        zip_code='78702', telephone_number='5127018287', restaurant_category=x)
y.save()
restaurants = Restaurant.objects.all()
restaurant = restaurants[0]


# reset Restaurant
def reset_table(model):
    from utils import delete_all
    delete_all(model)
    objects = model.objects.all()
    assert len(objects) == 0
    
reset_table(Restaurant)
reset_table(RestaurantCategory)