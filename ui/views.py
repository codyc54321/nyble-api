from django.shortcuts import render


def order_page(request):
    return render(request, 'ui/order-page.html')


def example_order_page(request):
    return render(request, 'ui/example_order_page.html')
