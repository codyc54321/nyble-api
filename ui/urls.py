from django.conf.urls import url, include
from ui import views

urlpatterns = [
    url(r'^order-page/', views.order_page, name='order-page'),
    url(r'^example-order-page/', views.example_order_page, name='example-order-page'),
]
