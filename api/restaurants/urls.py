from django.conf.urls import url, include
from api.restaurants import views

urlpatterns = [
    url(r'^instance/(?P<pk>[\d]+)', views.RestaurantInstanceView.as_view(), name='restaurant-instance'),
    url(r'^list', views.RestaurantListView.as_view(), name='restaurants-list'),
    
    url(r'^categories', views.RestaurantCategoryListView.as_view(), name='restaurant-categories-list'),
]