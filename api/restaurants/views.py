import json

from rest_framework import generics
from rest_framework.response import Response

from restaurants.serializers import RestaurantSerializer, RestaurantCategorySerializer
from restaurants.models import Restaurant, RestaurantCategory



class RestaurantListView(generics.ListAPIView):
    """
    List all restaurants
    """
    model = Restaurant
    serializer_class = RestaurantSerializer
    
    def get_queryset(self):
        return Restaurant.objects.all()
    

class RestaurantInstanceView(generics.RetrieveAPIView):
    """
    Returns a single Restaurant.
    Also allows updating and deleting
    """
    model = Restaurant
    serializer_class = RestaurantSerializer
    
    def get_queryset(self):
        pk = self.kwargs['pk']
        print(pk)
        return Restaurant.objects.filter(id=self.kwargs['pk'])
        
        
class RestaurantCategoryListView(generics.ListAPIView):
    """
    Returns a single RestaurantType.
    Also allows updating and deleting
    """
    model = RestaurantCategory
    serializer_class = RestaurantCategorySerializer
    
    def get_queryset(self):
        return RestaurantCategory.objects.all()