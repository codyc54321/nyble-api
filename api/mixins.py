from copy import copy
import json

from django.contrib.auth.models import User
from django.test import TransactionTestCase
from rest_framework.test import APIClient, APIRequestFactory

from model_mommy import mommy

from users.models import AppUser


class AppUserViewsTests(TransactionTestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.email = "fake@fake.com"
        self.password = "password"
        self.phone_number = "8135452150"
        self.base_data = {
            "first_name": "John",
            "last_name": "Doe",
            "username": self.email,
            "password": self.password,
        }

    def tearDown(self):
        self.delete_test_user()

    def run_assertion_test(self, url=None, data=None, method='post', expected_status_code=None, rejected_status_code=None, headers={}, **kwargs):
        client = APIClient(**headers)
        response = getattr(client, method)(url, data=data, **kwargs)
        if expected_status_code:
            self.assertEqual(response.status_code, expected_status_code)
        elif rejected_status_code:
            self.assertNotEqual(response.status_code, rejected_status_code)
        else:
            raise Exception("Give a status_code you expect, or one you don't want")
        return response

    def old_run_assertion_test(self, url=None, method='post', data=None, expected_status_code=None, **kwargs):
        response = self.call_view(url, method=method, data=data, extra_headers=None)
        self.assertEqual(response.status_code, expected_status_code)
        return response

    def call_view(self, url, method='post', data=None, **kwargs):
        request = getattr(self.factory, method)(url, data=json.dumps(data), content_type='application/json', **kwargs)# format='json')
        response = self.view(request)
        return response

    def create_user_and_app_user(self):
        try:
            user = mommy.make(User, **self.user_creation_data)
            mommy.make(AppUser, user=user, phone_number=self.phone_number)
        except:
            pass

    def delete_test_user(self):
        try:
            app_user = AppUser.objects.get(user__username=self.email)
            app_user.delete()
        except:
            pass # all is well

    @property
    def registration_request_data(self):
        this_data = copy(self.base_data)
        this_data.update({'phone_number': self.phone_number})
        return this_data

    @property
    def user_creation_data(self):
        this_data = copy(self.base_data)
        this_data.update({'email': self.email})
        return this_data
