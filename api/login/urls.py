from django.conf.urls import url, include
from api.login import views

urlpatterns = [
    url(r'^customer-login/', views.customer_login, name='customer-login'),
    url(r'^test-class-view/', views.TestViewClass.as_view(), name='class-view-token-test'),
    url(r'^test-func-view/', views.test_function_view, name='func-view-token-test'),
]
