from functools import wraps

from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework import status
from rest_framework.response import Response
from rest_framework.authtoken.models import Token


""" didnt work """
# http://www.django-rest-framework.org/tutorial/4-authentication-and-permissions/#adding-required-permissions-to-views


class token_required(object):

    def __init__(self, f):
        self.f = f

    def __call__(self, *args, **kwargs):
        request = args[0]
        token_header = request.META.get('HTTP_AUTHORIZATION', '')
        if token_header:
            beginning = 'Token '
            if not token_header.startswith(beginning):
                return Response(status=status.HTTP_400_BAD_REQUEST)
            token = token_header.replace(beginning, '')

            try:
                token_obj = Token.objects.get(key=token)
                user = token_obj.user
                assert isinstance(user, User)
            except:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            request.token = token
            return self.f(request, *args[1:], **kwargs)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
