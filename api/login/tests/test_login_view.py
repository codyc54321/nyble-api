from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework.test import APIClient

from model_mommy import mommy

from api.login.views import customer_login
from api.mixins import AppUserViewsTests


class LoginViewTests(AppUserViewsTests):

    def setUp(self):
        super().setUp()
        self.view = customer_login
        # only works for superuser...wtf...
        # self.token_url = reverse('auth-token')
        self.token_url = reverse('api:login:customer-login')
        self.class_test_url = reverse('api:login:class-view-token-test')
        self.func_test_url = reverse('api:login:func-view-token-test')
        self.login_payload = {'username': self.email, 'password': self.password}
        self.create_user_and_app_user()

    """ only works for superuser...wtf... """
    # def test_auth_token_works(self):
    #     response = self.run_assertion_test(self.token_url, data=self.login_payload, expected_status_code=200)

    def test_getting_auth_token_works(self):
        response = self.run_assertion_test(self.token_url, data=self.login_payload, expected_status_code=200)

    def test_request_to_protected_class_view_without_token_header_given_fails(self):
        self.old_run_assertion_test(self.class_test_url, expected_status_code=400)
        self.run_assertion_test(self.class_test_url, rejected_status_code=200, headers={'HTTP_CONTENT_TYPE': 'application/json'})

    def test_request_to_protected_function_view_without_token_header_given_fails_400(self):
        self.old_run_assertion_test(self.func_test_url, expected_status_code=400)
        """ AssertionError: .accepted_renderer not set on Response """
        # self.run_assertion_test(self.func_test_url, rejected_status_code=200, headers={'HTTP_CONTENT_TYPE': 'application/json'})

    def test_protected_class_view_with_token_header_given_works(self):
        token_header = self.get_token_header()
        self.run_assertion_test(self.class_test_url, expected_status_code=200, headers={'HTTP_AUTHORIZATION': token_header})

    def test_protected_function_view_with_token_header_given_works(self):
        token_header = self.get_token_header()
        self.run_assertion_test(self.func_test_url, expected_status_code=200, headers={'HTTP_AUTHORIZATION': token_header})

    def get_token_header(self):
        response = self.call_view(self.token_url, data=self.login_payload)
        token = response.data['token']
        token_header = 'Token {}'.format(token)
        return token_header

    def test_bad_requests_raise_400(self):
        BAD_DATA = {"username": self.email, "nope, bad JSON": "no dice"}
        self.run_assertion_test(self.token_url, data=BAD_DATA, expected_status_code=400)

    def test_bad_password_returns_401(self):
        BAD_PASSWORD_DATA = {'username': self.email, 'password': 'asuusfuasfusfd blah blah'}
        self.run_assertion_test(self.token_url, data=BAD_PASSWORD_DATA, expected_status_code=401)

    def test_bad_email_returns_401(self):
        BAD_EMAIL_DATA = {'username': 'thisemaildontexist@example.com', 'password': 'password'}
        self.run_assertion_test(self.token_url, data=BAD_EMAIL_DATA, expected_status_code=401)

    def test_good_combo_returns_200(self):
        good_data = {'username': self.email, 'password': self.password}
        self.run_assertion_test(self.token_url, data=good_data, expected_status_code=200)
