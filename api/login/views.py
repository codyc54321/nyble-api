import json

from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.shortcuts import render
from django.utils.decorators import method_decorator

from rest_framework import status
from rest_framework.views import APIView
from rest_framework.decorators import api_view #, permission_classes
from rest_framework.response import Response

from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ParseError
from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated

from users.models import AppUser
from api.login.permissions import token_required


# TODO: http://jasonwyatt.co/post/40138193838/generate-hashed-passwords-and-salts-with-python
@api_view(['POST'])
def customer_login(request):
    """
    Try to login a customer (food orderer)
    """
    data = request.data

    try:
        username = data['username']
        password = data['password']
    except:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    try:
        user = User.objects.get(username=username, password=password)
    except:
        return Response(status=status.HTTP_401_UNAUTHORIZED)

    try:
        user_token = user.auth_token.key
    except:
        token_obj = Token.objects.create(user=user)
        user_token = token_obj.key

    data = {'token': user_token}
    return Response(data=data, status=status.HTTP_200_OK)


#----------------------------------------------------------------------------------------------------

""" to be used for tests """

# @method_decorator(token_required)
# @for_all_methods(token_required)
class TestViewClass(APIView):
    """
    authentication needed for this view
    """

    @method_decorator(token_required)
    def get(self, *args, **kwargs):
        return Response({'message': 'OK'}, status=status.HTTP_200_OK)

    @method_decorator(token_required)
    def post(self, request, format=None):
        return Response({'message': 'OK'}, status=status.HTTP_200_OK)


@token_required
@api_view(['GET', 'POST'])
def test_function_view(request):
    return Response({'message': 'OK'}, status=status.HTTP_200_OK)
