from django.conf.urls import url, include
from api.menu import views

urlpatterns = [
    url(r'^create-menu/', views.create_menu, name='create-menu'),
    url(r'^get-menu/', views.get_menu, name='get-menu'),
]
