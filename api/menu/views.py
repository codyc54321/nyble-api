import json

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response


FAKE_MENU = [
    {
        "category": "Tacos",
        "items": [
            {"name": "Pollo", "price": 2.00},
            {"name": "Barbacoa", "price": 2.25},
            {"name": "Chorizo", "price": 2.50},
        ]
    },
    {
        "category": "Burritos",
        "items": [
            {"name": "Pollo", "price": 6.00},
            {"name": "Barbacoa", "price": 6.25},
            {"name": "Chorizo", "price": 6.50},
        ]
    },
]


@api_view(['POST'])
def create_menu(request):
    """
    Make a brand new menu
    """
    data = json.loads(request.body.decode("utf-8"))
    # save menu to db
    return Response(status=status.HTTP_200_OK)


@api_view(['GET'])
def get_menu(request):
    """
    List menu for 1 restaurant
    """
    menu = json.dumps(FAKE_MENU)
    return Response({'menu': menu})


# http://127.0.0.1:8000/api/menu/get-menu
