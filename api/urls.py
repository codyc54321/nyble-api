from django.conf.urls import url, include
from orders import views

urlpatterns = [
    url(r'^orders/', include('api.orders.urls', namespace='orders')),
    url(r'^registration/', include('api.registration.urls', namespace='registration')),
    url(r'^login/', include('api.login.urls', namespace='login')),
    url(r'^restaurants/', include('api.restaurants.urls', namespace='restaurants')),
    url(r'^menu/', include('api.menu.urls', namespace='menu')),
]


"""
SINGLE URL API!!!!


ORDER_FUNCS = {'create': create_model}

MAPPER = {'orders': ORDER_FUNCS}



def create_object(Model, **data):
    new_object = Model(**data)
    new_object.save()


request = {'model': 'Order', 'task': 'create', 'order_id': 4, 'name': 'New order', 'customer_id': 235}

funcs_dict = MAPPER[request['purpose']]
my_func = funcs_dict[request['task']]
"""
