import json

from django.shortcuts import render

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from messager.texts import send_text
from orders.formatting_functions import FUNCTIONS


def unpack_json(request):
    body = request.body
    try:
        data = json.loads(body)
    except TypeError:
        str_response = response.readall().decode('utf-8')
        obj = json.loads(str_response)


@api_view(['POST'])
def submit_order(request):
    """
    Text an order to a restaurant's phone
    """
    data = json.loads(request.body.decode("utf-8"))
    print(data)
    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
def text_order(request):
    """
    Text an order to a restaurant's phone
    """
    data = json.loads(request.body.decode("utf-8"))
    callback = FUNCTIONS[data['restaurant']]
    send_text(data, callback)
    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
def text_email(request):
    """
    Text an order to a restaurant's email account(s)
    """
    data = json.loads(request.body)
    # TODO: do email stuff
    callback = FUNCTIONS[data['restaurant']]
    send_text(data, callback)
    return Response(status=status.HTTP_200_OK)


JSON = {"customer_name": "Cody Childers", "order": [{"categoria": "taco", "ingredientes": "todos", "cantidad": 1, "relleno": "barbacoa", "envoltorio": "harina"}, {"categoria": "taco", "ingredientes": "todos", "cantidad": 2, "relleno": "pollo", "envoltorio": "harina"}, {"categoria": "burrito", "ingredientes": "todos", "cantidad": 1, "relleno": "pollo"}], "customer_phone_number": "8135452150", "restaurant": "las_trancas"}

#english
JSON = {"customer_name": "Cody Childers", "order": [{"category": "taco", "ingredients": "todos", "quantity": 1, "relleno": "barbacoa", "wrapping": "harina"}, {"category": "taco", "ingredients": "todos", "quantity": 2, "relleno": "pollo", "wrapping": "harina"}, {"category": "burrito", "ingredients": "todos", "quantity": 1, "relleno": "pollo"}], "customer_phone_number": "8135452150", "restaurant": "las_trancas"}


REQUEST = {
    'restaurant': 'las_trancas',
    'customer_name': 'Cody Childers',
    'customer_phone_number': '8135452150',
    'order': [
        {'categoria': 'taco', 'relleno': 'barbacoa', 'ingredientes': 'todos', 'envoltorio': 'harina', 'cantidad': 1},
        {'categoria': 'taco', 'relleno': 'pollo', 'ingredientes': 'todos', 'envoltorio': 'harina', 'cantidad': 2},
        {'categoria': 'burrito', 'relleno': 'pollo', 'ingredientes': 'todos', 'cantidad': 1},
    ],
}
