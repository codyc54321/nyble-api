from django.conf.urls import url, include
from api.orders import views

urlpatterns = [
    url(r'^text-order/', views.text_order, name='text-order'),
    url(r'^submit-order/', views.submit_order, name='submit-order'),
]
