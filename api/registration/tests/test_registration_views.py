from django.contrib.auth.models import User
from django.urls import reverse

from users.models import AppUser
from api.registration.views import register_user
from api.mixins import AppUserViewsTests


# http://stackoverflow.com/questions/21458387/transactionmanagementerror-you-cant-execute-queries-until-the-end-of-the-atom
# https://docs.djangoproject.com/en/1.10/topics/testing/tools/#django.test.TransactionTestCase
class RegistrationViewTests(AppUserViewsTests):

    def setUp(self):
        super().setUp()
        self.view = register_user
        self.url = reverse('api:registration:register-user')

    def test_making_new_users_that_didnt_exist(self):
        self.call_view(self.url, data=self.registration_request_data)
        user = User.objects.get(username=self.email)
        app_user = AppUser.objects.get(user__email=self.email)
        self.assertEqual(app_user.phone_number, self.phone_number)

    def test_bad_requests_raise_400(self):
        BAD_DATA = {"really bad key": "oops", "email": self.email,
                    "password": "password", "nope, bad JSON": "John"}
        self.run_assertion_test(self.url, data=BAD_DATA, method='post', expected_status_code=400)

    def test_user_that_existed_and_given_same_login_info_returns_307(self):
        """ if a user forgets they have an account and gives same username + password, log them in """
        self.create_user_and_app_user()
        self.run_assertion_test(self.url, data=self.registration_request_data, method='post', expected_status_code=307)

    def test_user_that_existed_and_given_wrong_login_info_returns_406(self):
        """ if a user forgets they have an account and gives same username + password, log them in """
        self.create_user_and_app_user()
        wrong_password_data = self.registration_request_data
        wrong_password_data['password'] = 'gizandapiss'
        self.run_assertion_test(self.url, data=wrong_password_data, method='post', expected_status_code=406)
