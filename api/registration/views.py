import json

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.shortcuts import render

from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.authtoken.models import Token

from users.models import AppUser
from utils import get_response_json


@api_view(['POST'])
def register_user(request):
    """
    Try to register a new user
    """

    data = request.data
    try:
        username = data['username'] # we use as email later
        password = data['password']
        KEYS = ['first_name', 'last_name', 'phone_number']
        for key in KEYS:
            _ = data[key]
    except:
        return Response(status=status.HTTP_400_BAD_REQUEST)

    # check on the login side (iOS, android, HTML, etc) that the username picked is actually an email. that way we can email them with the username
    user_already_exists = User.objects.filter(username=username)
    if user_already_exists:
        user = User.objects.filter(username=username, password=password)
        return Response(status=status.HTTP_307_TEMPORARY_REDIRECT) if user else Response(status=status.HTTP_406_NOT_ACCEPTABLE)

    data.update({'email': username})
    phone_number = data['phone_number']
    data.pop('phone_number', None)
    new_user = User.objects.create(**data)
    new_app_user = AppUser.objects.create(user=new_user, phone_number=phone_number)
    token = Token.objects.create(user=new_user)
    return Response(status=status.HTTP_200_OK)


"""
'{"last_name": "Doe", "email": "fake@fake.com", "password": "password", "phone": "8135452150", "first_name": "John"}'

{"first_name": "John", "last_name": "Doe", "email" : "test@example.com", "password": "password", "phone": "8135452150"}

"""
