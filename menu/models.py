from __future__ import unicode_literals

from django.db import models
from restaurants.models import Restaurant


"""
Christ

menus need to differentiate between toppings and fillings (sour cream vs ground beef)

fillings can sometimes be a topping, kinda...if you already filled a burrito with pork, but now want chicken as extra meat

maybe fillings can have an 'extra meat option' for certain items

some toppings are free, some cost extra

"""


"""
creating the menu:
    - allow creating categories, adding items, declaring toppings/fillings, splitting the menu into more menus (breakfast, lunch, dinner), all on same screen
    - offer a wizard to walk them through it

"""

class Category(models.Model):
    name = models.CharField(max_length=250)


class FoodItem(models.Model):
    name = models.CharField(max_length=250)
    restaurant = models.ForeignKey(Restaurant)
    category = models.ForeignKey(Category)


class Topping(models.Model):
    item = models.ForeignKey(FoodItem)
    price = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    restaurant = models.ForeignKey(Restaurant)


class Filling(models.Model):
    item = models.ForeignKey(FoodItem)
    restaurant = models.ForeignKey(Restaurant)


class MenuItem(models.Model):
    name = models.CharField(max_length=250)
    restaurant = models.ForeignKey(Restaurant)


class MenuItemFilling(models.Model):
    menu_item = models.ForeignKey(MenuItem)
    filling = models.ForeignKey(Filling)
    restaurant = models.ForeignKey(Restaurant)
    extra_price = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)


class MenuItemTopping(models.Model):
    menu_item = models.ForeignKey(MenuItem)
    topping = models.ForeignKey(Topping)
    restaurant = models.ForeignKey(Restaurant)
    price = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)


class Menu(models.Model):
    name = models.CharField(max_length=250)
    restaurant = models.ForeignKey(Restaurant)
