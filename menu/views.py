from django.shortcuts import render


def create_menu(request):
    return render(request, 'create_menu.html')