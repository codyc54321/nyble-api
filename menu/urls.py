from django.conf.urls import url, include
from menu import views

urlpatterns = [
    url(r'^create/', views.create_menu, name='create-menu'),
]