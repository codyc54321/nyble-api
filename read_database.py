#!/usr/bin/env python

"""
http://stackoverflow.com/questions/35226883/python-django-typeerror-relative-imports-require-the-package-argument
"""

import django, os
from django.core import serializers

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "nyble-api.settings")
# http://stackoverflow.com/questions/34114427/django-upgrading-to-1-9-error-appregistrynotready-apps-arent-loaded-yet
django.setup()

models = django.apps.apps.get_models()

BANNED_NAMES = ['Permission', 'ContentType']

MODEL_NAME_TO_FIELD_MAPPER = {
    'User': ('email', 'password')
}


def spaces_needed(fields, current_field):
    longest_field = max(fields, key=len) # email, password
    max_length = len(longest_field) # 8 (password)
    spaces_to_add = max_length - len(current_field) # 8 - len(email) = 3
    return spaces_to_add * ' '


def get_desired_model_fields(model_name):
    try:
        fields = MODEL_NAME_TO_FIELD_MAPPER[model_name]
        return None if fields == '__all__' else fields # all declaring {'ThisModel': '__all__'}
    except:
        pass


# http://stackoverflow.com/questions/2170228/iterate-over-model-instance-field-names-and-values-in-template
for Model in models:
    model_name = Model.__name__
    if model_name in BANNED_NAMES:
        continue
    if Model.objects.count():
        print('\nObjects in {class_name}:\n'.format(class_name=model_name))
        objects = Model.objects.all()[:5]
        desired_fields = get_desired_model_fields(model_name)
        data = serializers.serialize('python', objects, fields=desired_fields)
        for instance in data:
            # import ipdb; ipdb.set_trace()
            fields_dict = instance['fields'] # {'email': 'cchilder@mail.usf.edu', 'password': 'childers9'}
            fields = fields_dict.keys() # email, password

            for field, value in fields_dict.items():
                # if desired_fields:
                #     pass
                #     # print it in order, like Email then Password
                print('  {field_name}: {extra_spaces}{value}'.format(field_name=field.capitalize(), extra_spaces=spaces_needed(fields, field), value=value))
            print('')
